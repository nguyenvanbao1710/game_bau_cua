import { configureStore } from "@reduxjs/toolkit";
import gameBauCuaSlice from "./slices/gameBauCuaSlice";

export const store = configureStore({
    reducer: {
        gameBauCuaSlice,
    },
});
