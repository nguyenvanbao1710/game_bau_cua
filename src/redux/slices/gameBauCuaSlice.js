import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    danhSachCuoc: [
        { ma: "nai", hinhAnh: "./img/nai.png", diemCuoc: 0 },
        { ma: "bau", hinhAnh: "./img/bau.png", diemCuoc: 0 },
        { ma: "ga", hinhAnh: "./img/ga.png", diemCuoc: 0 },
        { ma: "ca", hinhAnh: "./img/ca.png", diemCuoc: 0 },
        { ma: "cua", hinhAnh: "./img/cua.png", diemCuoc: 0 },
        { ma: "tom", hinhAnh: "./img/tom.png", diemCuoc: 0 },
    ],
    tongDiem: 2500,
    mangXucXac: [
        { ma: "bau", hinhAnh: "./img/bau.png" },
        { ma: "ga", hinhAnh: "./img/ga.png" },
        { ma: "ca", hinhAnh: "./img/ca.png" },
    ],
};

const gameBauCuaSlice = createSlice({
    name: "gameBauCua",
    initialState,
    reducers: {
        datCuoc: (state, action) => {
            let index = state.danhSachCuoc.findIndex(
                (qc) => qc.ma === action.payload.quanCuoc.ma
            );

            if (index !== -1) {
                if (action.payload.tangGiam) {
                    if (state.tongDiem > 0) {
                        state.danhSachCuoc[index].diemCuoc += 100;
                        state.tongDiem -= 100;
                    }
                } else {
                    if (state.danhSachCuoc[index].diemCuoc > 0) {
                        state.danhSachCuoc[index].diemCuoc -= 100;
                        state.tongDiem += 100;
                    }
                }
            }
        },
        playGame: (state, action) => {
            const mangXucXacNgauNhien = [];
            for (let i = 0; i < 3; i++) {
                let soNgauNhien = Math.floor(Math.random() * 6);
                const xucXacNgauNhien = state.danhSachCuoc[soNgauNhien];
                mangXucXacNgauNhien.push(xucXacNgauNhien);
            }
            state.mangXucXac = mangXucXacNgauNhien;
            //xử lý điểm thường
            mangXucXacNgauNhien.forEach((item, index) => {
                const indexDSC = state.danhSachCuoc.findIndex(
                    (qc) => qc.ma === item.ma
                );
                if (indexDSC !== -1) {
                    state.tongDiem += state.danhSachCuoc[indexDSC].diemCuoc;
                }
            });
            //xử lý hoàn tiền
            state.danhSachCuoc.forEach((qc, index) => {
                let indexXXNN = mangXucXacNgauNhien.findIndex(
                    (xxnn) => xxnn.ma === qc.ma
                );
                if (indexXXNN !== -1) {
                    state.tongDiem += qc.diemCuoc;
                }
            });
            //xử lý làm mới game
            state.danhSachCuoc = state.danhSachCuoc.map((qc, index) => {
                return { ...qc, diemCuoc: 0 };
            });
        },
        rePlay: (state, action) => {
            state.tongDiem = 2500;
            state.danhSachCuoc = state.danhSachCuoc.map((qc, item) => {
                return { ...qc, diemCuoc: 0 };
            });
        },
    },
});

export default gameBauCuaSlice.reducer;
export const { datCuoc, playGame, rePlay } = gameBauCuaSlice.actions;
