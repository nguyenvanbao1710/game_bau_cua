import React from "react";
import { useDispatch } from "react-redux";
import { useSpring, animated } from "react-spring";
import { datCuoc } from "../redux/slices/gameBauCuaSlice";

export default function QuanCuoc(props) {
    const { quanCuoc } = props;
    const propsUseSpring = useSpring({ to: { scale: 1 }, from: { scale: 0 } });
    const dispatch = useDispatch();

    return (
        <div className="mt-4 text-center">
            <img src={quanCuoc.hinhAnh} alt="" style={{ height: "150px" }} />
            <div className="mt-2 text-center">
                <animated.button
                    style={{
                        transform: propsUseSpring.scale.interpolate(
                            (scale) => `scale(${scale})`
                        ),
                    }}
                    className="btn btn-danger"
                    onClick={() => {
                        dispatch(
                            datCuoc({
                                quanCuoc,
                                tangGiam: true,
                            })
                        );
                    }}
                >
                    <i className="fa fa-plus"></i>
                </animated.button>
                <span className="text-warning mx-2">{quanCuoc.diemCuoc}</span>
                <animated.button
                    style={{
                        transform: propsUseSpring.scale.interpolate(
                            (scale) => `scale(${scale})`
                        ),
                    }}
                    onClick={() => {
                        dispatch(
                            datCuoc({
                                quanCuoc,
                                tangGiam: false,
                            })
                        );
                    }}
                    className="btn btn-danger"
                >
                    <i className="fa-solid fa-minus"></i>
                </animated.button>
            </div>
        </div>
    );
}
