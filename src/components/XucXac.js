import React, { Fragment } from "react";
import { useSpring, animated } from "react-spring";
export default function XucXac(props) {
    let { xucXacItem } = props;

    const [propsDice, set] = useSpring(() => ({
        to: {
            xyz: [360, 360, 360],
        },
        from: {
            xyz: [0, 0, 0],
        },
        config: {
            duration: 500,
        },
        reset: true,
    }));

    set({ xyz: [360, 360, 360] });

    return (
        <Fragment>
            <animated.div
                className="scene"
                style={{
                    transform: propsDice.xyz.interpolate(
                        (x, y, z) =>
                            `translateZ(-25px) rotateX(${x}deg) rotateY(${y}deg) rotateZ(${z}deg)`
                    ),
                }}
            >
                <div className="cube">
                    <img
                        className="cube__face front"
                        src={xucXacItem.hinhAnh}
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                    <img
                        className="cube__face back"
                        src="./img/cua.png"
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                    <img
                        className="cube__face left"
                        src="./img/ga.png"
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                    <img
                        className="cube__face right"
                        src="./img/nai.png"
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                    <img
                        className="cube__face top"
                        src="./img/tom.png"
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                    <img
                        className="cube__face bottom"
                        src="./img/ca.png"
                        alt=""
                        style={{
                            width: "50px",
                            marginTop: "0px",
                            marginLeft: "0px",
                        }}
                    />
                </div>
            </animated.div>
        </Fragment>
    );
}
