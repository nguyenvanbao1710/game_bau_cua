import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { playGame } from "../redux/slices/gameBauCuaSlice";
import XucXac from "./XucXac";

export default function DanhSachXucXac(props) {
    const dispatch = useDispatch();

    const mangXucXac = useSelector((state) => state.gameBauCuaSlice.mangXucXac);
    return (
        <div className="text-center">
            <div
                className="bg-white mt-5 ml-5"
                style={{ width: "300px", height: "300px", borderRadius: "50%" }}
            >
                <div className="row">
                    <div
                        className="col-12 pt-2"
                        style={{ marginLeft: "90px", marginTop: "-40px" }}
                    >
                        <XucXac xucXacItem={mangXucXac[0]} />
                    </div>
                    <div className="col-6 pl-5">
                        <XucXac xucXacItem={mangXucXac[1]} />
                    </div>
                    <div className="col-6">
                        <XucXac xucXacItem={mangXucXac[2]} />
                    </div>
                </div>
            </div>
            <div className="text-center mt-3" style={{ marginRight: "70px" }}>
                <button
                    onClick={() => {
                        dispatch(playGame());
                    }}
                    className="btn-play"
                >
                    Xốc
                </button>
            </div>
        </div>
    );
}
