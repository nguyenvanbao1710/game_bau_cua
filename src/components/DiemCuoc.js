import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { rePlay } from "../redux/slices/gameBauCuaSlice";

export default function DiemCuoc() {
    const tongDiem = useSelector((state) => state.gameBauCuaSlice.tongDiem);
    const dispatch = useDispatch();

    return (
        <div>
            <h1 className="text-center pt-3 display-4">GAME BẦU CUA</h1>
            <div className="text-center">
                <h3 className="text-center">
                    Tiền thưởng:{" "}
                    <span className="text-warning">
                        {tongDiem.toLocaleString()}$
                    </span>
                </h3>
            </div>
            <div className="text-center mt-2">
                <button
                    className="btn-replay"
                    onClick={() => {
                        dispatch(rePlay());
                    }}
                >
                    Chơi lại
                </button>
            </div>
        </div>
    );
}
